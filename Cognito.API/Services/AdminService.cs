﻿using Amazon;
using Amazon.CognitoIdentityProvider;
using Amazon.CognitoIdentityProvider.Model;
using Cognito.API.Models.Requests;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Cognito.API.Services
{
    public interface IAdminService
    {
        Task<AdminInitiateAuthResponse> SignInAsync(LoginRequest loginModel);
        Task<AdminUpdateUserAttributesResponse> UpdateUserAsync(AdminUpdateUserRequest updateUserModel);
        Task<AdminConfirmSignUpResponse> ConfirmSignUpAsync(LoginRequest loginModel);
        Task<AdminDeleteUserResponse> DeleteUserAsync(LoginRequest loginModel);
        Task<AdminCreateUserResponse> RegisterUserAsync(AdminNewUserRequest newUserRequest);
        Task<AdminSetUserPasswordResponse> ChangeUserPasswordAsync(LoginRequest loginModel);
    }
    public class AdminService: IAdminService
    {
        private readonly RegionEndpoint _region;
        private readonly AmazonCognitoIdentityProviderClient _provider;

        public AdminService()
        {
            _region = AppSettings.Region;
            _provider = new AmazonCognitoIdentityProviderClient(AppSettings.AdminAccessKeyID, AppSettings.AdminSecretAccessKey, _region);
        }

        public async Task<AdminInitiateAuthResponse> SignInAsync(LoginRequest loginModel)
        {
            var authParams = new Dictionary<string, string>();
            authParams.Add("USERNAME", loginModel.Username);
            authParams.Add("PASSWORD", loginModel.Password);

            var authResponse = await _provider.AdminInitiateAuthAsync(new AdminInitiateAuthRequest()
            {
                AuthFlow = AuthFlowType.ADMIN_NO_SRP_AUTH,
                AuthParameters = authParams,
                UserPoolId = AppSettings.UserPoolID,
                ClientId = AppSettings.AppClientID
            });

            return authResponse;
        }

        public async Task<AdminCreateUserResponse> RegisterUserAsync(AdminNewUserRequest newUserRequest)
        {
            var attributes = new List<AttributeType>();

            if (!string.IsNullOrWhiteSpace(newUserRequest.Email))
            {
                attributes.Add(new AttributeType() { Name = Constants.Email, Value = newUserRequest.Email });
            }
            if (!string.IsNullOrWhiteSpace(newUserRequest.Role))
            {
                attributes.Add(new AttributeType() { Name = Constants.Role, Value = newUserRequest.Role });
            }
            if (newUserRequest.Id > 0)
            {
                attributes.Add(new AttributeType() { Name = Constants.Id, Value = newUserRequest.Id.ToString() });
            }

            var response = await _provider.AdminCreateUserAsync(new AdminCreateUserRequest()
            {
                MessageAction = "SUPPRESS", // SUPPRESS means do not send email or sms                
                Username = newUserRequest.Username,
                TemporaryPassword = "Th1sIsT3mp0rary!", // it won't be used
                UserAttributes = attributes,
                UserPoolId = AppSettings.UserPoolID
            });

            return response;
        }

        public async Task<AdminSetUserPasswordResponse> ChangeUserPasswordAsync(LoginRequest loginModel)
        {
            var response = await _provider.AdminSetUserPasswordAsync(new AdminSetUserPasswordRequest()
            {
                Username = loginModel.Username,
                Password = loginModel.Password,
                Permanent = true,
                UserPoolId = AppSettings.UserPoolID
            });

            return response;
        }

        public async Task<AdminUpdateUserAttributesResponse> UpdateUserAsync(AdminUpdateUserRequest updateUserRequest)
        {
            var attributes = new List<AttributeType>();

            if (updateUserRequest.Email != null)
            {
                attributes.Add(new AttributeType() { Name = Constants.Email, Value = updateUserRequest.Email });
            }
            if (updateUserRequest.EmailVerified != null)
            {
                attributes.Add(new AttributeType() { Name = Constants.EmailVerified, Value = updateUserRequest.EmailVerified });
            }
            if (updateUserRequest.FirstName != null)
            {
                attributes.Add(new AttributeType() { Name = Constants.FirstName, Value = updateUserRequest.FirstName });
            }
            if (updateUserRequest.LastName != null)
            {
                attributes.Add(new AttributeType() { Name = Constants.LastName, Value = updateUserRequest.LastName });
            }
            // add the rest here

            var updateUserAttributesRequest = new AdminUpdateUserAttributesRequest()
            {
                Username = updateUserRequest.Username,
                UserAttributes = attributes,
                UserPoolId = AppSettings.UserPoolID,
            };
                        
            var result = await _provider.AdminUpdateUserAttributesAsync(updateUserAttributesRequest);

            return result;
        }

        public async Task<AdminConfirmSignUpResponse> ConfirmSignUpAsync(LoginRequest loginModel)
        {
            var response = await _provider.AdminConfirmSignUpAsync(new AdminConfirmSignUpRequest()
            {
                UserPoolId = AppSettings.UserPoolID,
                Username = loginModel.Username
            });

            return response;
        }

        public async Task<AdminDeleteUserResponse> DeleteUserAsync(LoginRequest loginModel)
        {
            var response = await _provider.AdminDeleteUserAsync(new AdminDeleteUserRequest()
            {                
                UserPoolId = AppSettings.UserPoolID,
                Username = loginModel.Username
            });

            return response;
        }
    }
}