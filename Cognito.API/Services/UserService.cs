﻿using Amazon;
using Amazon.CognitoIdentityProvider;
using Amazon.CognitoIdentityProvider.Model;
using Amazon.Extensions.CognitoAuthentication;
using Amazon.Runtime;
using Cognito.API.Models.Requests;
using Cognito.API.Models.Response;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AWS = Amazon.CognitoIdentityProvider.Model;

namespace Cognito.API.Services
{
    public interface IUserService
    {
        Task<UserResponse> GetUserAsync(string accessToken);
        Task<AuthFlowResponse> SignInAsync(LoginRequest loginModel);
        Task<AuthFlowResponse> SmsMfaAsync(LoginMfaRequest loginModel);
        Task ResendSmsMfaAsync(string username);
        Task<AuthFlowResponse> NewPasswordAsync(LoginNewPasswordRequest loginModel);
        Task<SignUpResponse> SignUpAsync(Models.Requests.SignUpRequest signUpModel);
        Task ConfirmSignUpAsync(MfaCodeConfirmRequest mfaCodeConfirmModel);
        Task<UpdateUserAttributesResponse> UpdateUserAsync(UpdateUserRequest updateUserModel);
        Task<SetUserSettingsResponse> EnableUserMfaAsync(string accessToken);
    }
    public class UserService: IUserService
    {
        private readonly RegionEndpoint _region;
        private readonly AmazonCognitoIdentityProviderClient _provider;
        private readonly CognitoUserPool _userPool;

        public UserService()
        {
            _region = AppSettings.Region;

            _provider = new AmazonCognitoIdentityProviderClient(new AnonymousAWSCredentials(), _region);
            _userPool = new CognitoUserPool(AppSettings.UserPoolID, AppSettings.AppClientID, _provider,
                !string.IsNullOrWhiteSpace(AppSettings.AppClientSecret) ? AppSettings.AppClientSecret : null);
        }

        public async Task<UserResponse> GetUserAsync(string accessToken)
        {
            var getUserRequest = new GetUserRequest();            
            getUserRequest.AccessToken = accessToken;

            var getUserReponse = await _provider.GetUserAsync(getUserRequest);

            UserResponse user = null;
            if (getUserReponse != null)
            {
                user = new UserResponse()
                {
                    Username = getUserReponse.Username,
                    Email = getUserReponse.UserAttributes.Where(item => item.Name == Constants.Email)?.FirstOrDefault()?.Value,
                    EmailVerified = getUserReponse.UserAttributes.Where(item => item.Name == Constants.EmailVerified)?.FirstOrDefault()?.Value,
                    PhoneNumber = getUserReponse.UserAttributes.Where(item => item.Name == Constants.PhoneNumber)?.FirstOrDefault()?.Value,
                    PhoneNumberVerified = getUserReponse.UserAttributes.Where(item => item.Name == Constants.PhoneNumberVerified)?.FirstOrDefault()?.Value,
                    Address = getUserReponse.UserAttributes.Where(item => item.Name == Constants.Address)?.FirstOrDefault()?.Value,
                    BirthDate = getUserReponse.UserAttributes.Where(item => item.Name == Constants.BirthDate)?.FirstOrDefault()?.Value,
                    LastName = getUserReponse.UserAttributes.Where(item => item.Name == Constants.LastName)?.FirstOrDefault()?.Value,
                    Gender = getUserReponse.UserAttributes.Where(item => item.Name == Constants.Gender)?.FirstOrDefault()?.Value,
                    FirstName = getUserReponse.UserAttributes.Where(item => item.Name == Constants.FirstName)?.FirstOrDefault()?.Value,
                    Locale = getUserReponse.UserAttributes.Where(item => item.Name == Constants.Locale)?.FirstOrDefault()?.Value,
                    MiddleName = getUserReponse.UserAttributes.Where(item => item.Name == Constants.MiddleName)?.FirstOrDefault()?.Value,
                    Name = getUserReponse.UserAttributes.Where(item => item.Name == Constants.Name)?.FirstOrDefault()?.Value,
                    Nickname = getUserReponse.UserAttributes.Where(item => item.Name == Constants.Nickname)?.FirstOrDefault()?.Value,
                    Picture = getUserReponse.UserAttributes.Where(item => item.Name == Constants.Picture)?.FirstOrDefault()?.Value,
                    PreferredUsername = getUserReponse.UserAttributes.Where(item => item.Name == Constants.PreferredUsername)?.FirstOrDefault()?.Value,
                    Profile = getUserReponse.UserAttributes.Where(item => item.Name == Constants.Profile)?.FirstOrDefault()?.Value,
                    ZoneInfo = getUserReponse.UserAttributes.Where(item => item.Name == Constants.ZoneInfo)?.FirstOrDefault()?.Value,
                    UpdatedAt = getUserReponse.UserAttributes.Where(item => item.Name == Constants.UpdatedAt)?.FirstOrDefault()?.Value,
                    Website = getUserReponse.UserAttributes.Where(item => item.Name == Constants.Website)?.FirstOrDefault()?.Value,
                    JJUsername = getUserReponse.UserAttributes.Where(item => item.Name == Constants.JmsUsername)?.FirstOrDefault()?.Value,
                    Role = getUserReponse.UserAttributes.Where(item => item.Name == Constants.Role)?.FirstOrDefault()?.Value,
                };
            }

            return user;
        }

        public async Task<AuthFlowResponse> SignInAsync(LoginRequest loginModel)
        {
            var cognitoUser = new CognitoUser(loginModel.Username, AppSettings.AppClientID, _userPool, _provider,
                !string.IsNullOrWhiteSpace(AppSettings.AppClientSecret) ? AppSettings.AppClientSecret : null);

            var authRequest = new InitiateSrpAuthRequest()
            {
                Password = loginModel.Password
            };

            var authResponse = await cognitoUser.StartWithSrpAuthAsync(authRequest);

            return authResponse;
        }

        public async Task<SignUpResponse> SignUpAsync(Models.Requests.SignUpRequest signUpModel)
        {
            var attributes = new List<AttributeType>()
            {
                new AttributeType() { Name = Constants.Email, Value = signUpModel.Email},
                new AttributeType() { Name = Constants.JmsUsername, Value = signUpModel.Username},
                new AttributeType() { Name = Constants.PhoneNumber, Value = signUpModel.PhoneNumber}
            };

            var signUpRequest = new Amazon.CognitoIdentityProvider.Model.SignUpRequest()
            {
                ClientId = AppSettings.AppClientID,                
                Username = signUpModel.Username,
                Password = signUpModel.Password,
                UserAttributes = attributes,                
            };

            signUpRequest.UserAttributes = attributes;

            var result = await _provider.SignUpAsync(signUpRequest);

            return result;
        }

        public async Task ConfirmSignUpAsync(MfaCodeConfirmRequest mfaCodeConfirmModel)
        {
            var cognitoUser = new CognitoUser(mfaCodeConfirmModel.Username, AppSettings.AppClientID, _userPool, _provider, 
                !string.IsNullOrWhiteSpace(AppSettings.AppClientSecret) ? AppSettings.AppClientSecret : null);

            await cognitoUser.ConfirmSignUpAsync(mfaCodeConfirmModel.MfaCode, false);
        }

        public async Task<AuthFlowResponse> SmsMfaAsync(LoginMfaRequest loginModel)
        {
            var cognitoUser = new CognitoUser(loginModel.Username, AppSettings.AppClientID, _userPool, _provider, 
                !string.IsNullOrWhiteSpace(AppSettings.AppClientSecret) ? AppSettings.AppClientSecret : null);

            var mfaResponse = await cognitoUser.RespondToSmsMfaAuthAsync(new RespondToSmsMfaRequest()
            {
                SessionID = loginModel.SessionID,
                MfaCode = loginModel.MfaCode

            }).ConfigureAwait(false);

            return mfaResponse;
        }

        public async Task ResendSmsMfaAsync(string username)
        {
            var cognitoUser = new CognitoUser(username, AppSettings.AppClientID, _userPool, _provider,
                !string.IsNullOrWhiteSpace(AppSettings.AppClientSecret) ? AppSettings.AppClientSecret : null);

            await cognitoUser.ResendConfirmationCodeAsync();
        }

        public async Task<AuthFlowResponse> NewPasswordAsync(LoginNewPasswordRequest loginModel)
        {
            var cognitoUser = new CognitoUser(loginModel.Username, AppSettings.AppClientID, _userPool, _provider,
                !string.IsNullOrWhiteSpace(AppSettings.AppClientSecret) ? AppSettings.AppClientSecret : null);

            var authResponse = await cognitoUser.RespondToNewPasswordRequiredAsync(new RespondToNewPasswordRequiredRequest()
            {
                SessionID = loginModel.SessionID,
                NewPassword = loginModel.NewPassword
            });

            return authResponse;
        }

        public async Task<UpdateUserAttributesResponse> UpdateUserAsync(UpdateUserRequest updateUserModel)
        {
            var attributes = new List<AttributeType>();            

            if (updateUserModel.Email != null)
            {
                attributes.Add(new AttributeType() { Name = Constants.Email, Value = updateUserModel.Email });
            }
            if (updateUserModel.FirstName != null)
            {
                attributes.Add(new AttributeType() { Name = Constants.FirstName, Value = updateUserModel.FirstName });
            }
            if (updateUserModel.LastName != null)
            {
                attributes.Add(new AttributeType() { Name = Constants.LastName, Value = updateUserModel.LastName });
            }
            if (updateUserModel.PhoneNumber != null)
            {
                attributes.Add(new AttributeType() { Name = Constants.PhoneNumber, Value = updateUserModel.PhoneNumber });
            }
            if (updateUserModel.PreferredUsername != null)
            {
                attributes.Add(new AttributeType() { Name = Constants.PreferredUsername, Value = updateUserModel.PreferredUsername });
            }
            // add the rest here

            var updateUserAttributesRequest = new UpdateUserAttributesRequest()
            {
                AccessToken = updateUserModel.AccessToken,
                UserAttributes = attributes,
            };

            var result = await _provider.UpdateUserAttributesAsync(updateUserAttributesRequest);

            return result;
        }

        public async Task<SetUserSettingsResponse> EnableUserMfaAsync(string accessToken)
        {
            var mfas = new List<MFAOptionType>();            
            var mfa = new MFAOptionType()
            {
                AttributeName = Constants.PhoneNumber,
                DeliveryMedium = DeliveryMediumType.SMS
            };
            mfas.Add(mfa);

            var setUserSettingsRequest = new SetUserSettingsRequest()
            {
                MFAOptions = mfas,
                AccessToken = accessToken                
            };

            var setUserSettingsResponse = await _provider.SetUserSettingsAsync(setUserSettingsRequest);

            return setUserSettingsResponse;
        }

        public async Task<ChangePasswordResponse> EnableUserMfaAsync(string accessToken, string previousPassword, string newPassword)
        {
            var request = new AWS.ChangePasswordRequest()
            {
                AccessToken = accessToken,
                PreviousPassword = previousPassword,
                ProposedPassword = newPassword
            };

            var response = await _provider.ChangePasswordAsync(request);

            return response;
        }
    }
}