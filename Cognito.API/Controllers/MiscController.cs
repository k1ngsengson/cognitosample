﻿using Amazon.CognitoIdentityProvider.Model;
using Cognito.API.Models;
using Cognito.API.Models.Requests;
using Cognito.API.Services;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Cognito.API.Controllers
{
    public class MiscController : ApiController
    {
        private readonly IUserService _userService;

        public MiscController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpPost, Route("resendsmsmfa/{username}")]
        public async Task<HttpResponseMessage> ResendSmsMfa(string username)
        {
            try
            {
                await _userService.ResendSmsMfaAsync(username);

                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        [HttpPost, Route("newpassword")]

        public async Task<HttpResponseMessage> NewPassword([FromBody] LoginNewPasswordRequest login)
        {
            try
            {
                var authResponse = await _userService.NewPasswordAsync(login);

                var accessToken = authResponse.AuthenticationResult.AccessToken;

                var user = await _userService.GetUserAsync(accessToken);

                return Request.CreateResponse(HttpStatusCode.OK, user);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        [HttpPost, Route("enableusermfa")]
        public async Task<HttpResponseMessage> EnableUserMfa(string accessToken)
        {
            try
            {
                var result = await _userService.EnableUserMfaAsync(accessToken);

                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }
    }
}