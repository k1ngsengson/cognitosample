﻿using Amazon.CognitoIdentityProvider.Model;
using Cognito.API.Models.Requests;
using Cognito.API.Services;
using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Cognito.API.Controllers
{
    public class AccountController : ApiController
    {
        private readonly IUserService _userService;

        public AccountController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpPost, Route("signin")]
        public async Task<HttpResponseMessage> SignIn([FromBody] LoginRequest login)
        {
            try
            {
                var authResponse = await _userService.SignInAsync(login);              

                return Request.CreateResponse(HttpStatusCode.OK, authResponse);
            }
            catch (UserNotFoundException ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
            catch (UserNotConfirmedException ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        [HttpPost, Route("loginverifysms")]
        public async Task<HttpResponseMessage> SmsMfa([FromBody] LoginMfaRequest login)
        {
            try
            {
                var authResponse = await _userService.SmsMfaAsync(login);

                return Request.CreateResponse(HttpStatusCode.OK, authResponse);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        [HttpPost, Route("signup")]
        public async Task<HttpResponseMessage> SignUp([FromBody] Models.Requests.SignUpRequest signUp)
        {
            try
            {
                var result = await _userService.SignUpAsync(signUp);

                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        [HttpPost, Route("signupconfirm")]
        public async Task<HttpResponseMessage> SignUpConfirm([FromBody] MfaCodeConfirmRequest mfaCodeConfirm)
        {
            try
            {
                await _userService.ConfirmSignUpAsync(mfaCodeConfirm);

                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        [HttpGet, Route("getuser")]
        public async Task<HttpResponseMessage> GetUser(string accessToken)
        {
            try
            {
                var user = await _userService.GetUserAsync(accessToken);

                return Request.CreateResponse(HttpStatusCode.OK, user);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        [HttpPut, Route("updateuser")]
        public async Task<HttpResponseMessage> UpdateUser([FromBody] UpdateUserRequest updateUser)
        {
            try
            {
                var result = await _userService.UpdateUserAsync(updateUser);
                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }
    }
}

