﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Cognito.API.Controllers
{
    public class Item
    {
        public int Id { get; set; }
        public string StringId { get; set; }
        public string Name { get; set; }
    }
    public class ValuesController : ApiController
    {
        private readonly List<Item> Items;
        public ValuesController()
        {
            Items.AddRange(new List<Item>() {
                new Item() { Id = 1, StringId = "1", Name = "A" },
                new Item() { Id = 2, StringId = "2", Name = "B" },
                new Item() { Id = 3, StringId = "3", Name = "C" },
                new Item() { Id = 4, StringId = "4", Name = "D" },
                new Item() { Id = 11, StringId = "11", Name = "E" },
                new Item() { Id = 21, StringId = "21", Name = "F" }
                }
             );
        }

        public IEnumerable<string> SortById()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        public void Post([FromBody] string value)
        {
        }

        // PUT api/values/5
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }
    }
}
