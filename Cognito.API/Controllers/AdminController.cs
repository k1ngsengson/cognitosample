﻿using Amazon.CognitoIdentityProvider.Model;
using Cognito.API.Models;
using Cognito.API.Models.Requests;
using Cognito.API.Services;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Cognito.API.Controllers
{
    public class AdminController : ApiController
    {        
        private readonly IAdminService _adminService;        

        public AdminController(IAdminService adminService)
        {
            _adminService = adminService;
        }

        [HttpPost, Route("adminsignin")]
        public async Task<HttpResponseMessage> SignIn([FromBody] LoginRequest login)
        {
            try
            {
                var authResponse = await _adminService.SignInAsync(login);

                return Request.CreateResponse(HttpStatusCode.OK, authResponse);
            }
            catch (UserNotFoundException ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
            catch (UserNotConfirmedException ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        [HttpPut, Route("adminupdateuser")]
        public async Task<HttpResponseMessage> UpdateUser([FromBody] AdminUpdateUserRequest updateUser)
        {
            try
            {
                var result = await _adminService.UpdateUserAsync(updateUser);
                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        [HttpPut, Route("adminconfirmsignup")]
        public async Task<HttpResponseMessage> ConfirmSignUp([FromBody] LoginRequest login)
        {
            try
            {
                var result = await _adminService.ConfirmSignUpAsync(login);
                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        [HttpPut, Route("deleteuser")]
        public async Task<HttpResponseMessage> DeleteUser([FromBody] LoginRequest login)
        {
            try
            {
                var result = await _adminService.DeleteUserAsync(login);
                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        [HttpPost, Route("RegisterUser")]
        public async Task<HttpResponseMessage> RegisterUser([FromBody] AdminNewUserRequest newUser)
        {
            try
            {
                var result = await _adminService.RegisterUserAsync(newUser);
                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        [HttpPost, Route("ChangeUserPassword")]
        public async Task<HttpResponseMessage> ChangeUserPassword([FromBody] LoginRequest login)
        {
            try
            {
                var result = await _adminService.ChangeUserPasswordAsync(login);
                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }
    }
}