﻿namespace Cognito.API
{
    internal static class Constants
    {
        #region Email scope
        public const string Email = "email";
        public const string EmailVerified = "email_verified";
        #endregion

        #region Address scope
        public const string Address = "address";
        #endregion

        #region Phone scope
        public const string PhoneNumber = "phone_number";
        public const string PhoneNumberVerified = "phone_number_verified";
        #endregion

        #region Profile scope
        public const string BirthDate = "birthdate";
        public const string LastName = "family_name";
        public const string Gender = "gender";
        public const string FirstName = "given_name";
        public const string Locale = "locale";
        public const string MiddleName = "middle_name";
        public const string Name = "name";
        public const string Nickname = "nickname";
        public const string Picture = "picture";
        public const string PreferredUsername = "preferred_username";
        public const string Profile = "profile";
        public const string ZoneInfo = "zoneinfo";
        public const string UpdatedAt = "updated_at";
        public const string Website = "website";        
        #endregion

        #region Custom attributes        
        public const string JmsUsername = "custom:jms_username";
        public const string Role = "custom:role";
        public const string Id = "custom:id";
        #endregion
    }
}

