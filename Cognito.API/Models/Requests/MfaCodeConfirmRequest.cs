﻿using Cognito.API.Models.Interfaces;

namespace Cognito.API.Models.Requests
{
    public class MfaCodeConfirmRequest: LoginModel, IUsername, IMfaCode
    {
        public string MfaCode { get; set; }
    }
}