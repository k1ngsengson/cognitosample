﻿using Cognito.API.Models.Interfaces;

namespace Cognito.API.Models.Requests
{    
    public class LoginRequest: LoginModel, IUsername, IPassword
    {   
        public string Password { get; set; }
    }
}