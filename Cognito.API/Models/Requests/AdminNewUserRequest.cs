﻿namespace Cognito.API.Models.Requests
{
    public class AdminNewUserRequest
    {        
        public string Username { get; set; }
        public int Id { get; set; }
        public string Role { get; set; }
        public string Email { get; set; }
    }
}