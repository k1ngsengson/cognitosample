﻿namespace Cognito.API.Models.Requests
{
    public class UpdateUserRequest: UserModel
    {
        public string AccessToken { get; set; }
    }
}