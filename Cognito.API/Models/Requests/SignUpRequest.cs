﻿using Cognito.API.Models.Interfaces;

namespace Cognito.API.Models.Requests
{
    public class SignUpRequest: LoginModel, IUsername, IPassword
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public string PhoneNumber { get; set; }
    }
}