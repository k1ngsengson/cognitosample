﻿using Cognito.API.Models.Interfaces;

namespace Cognito.API.Models.Requests
{
    public class LoginNewPasswordRequest : LoginModel, ISessionID
    {
        public string NewPassword { get; set; }
        public string SessionID { get; set; }
    }
}