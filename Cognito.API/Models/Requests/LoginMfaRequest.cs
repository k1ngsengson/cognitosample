﻿using Cognito.API.Models.Interfaces;

namespace Cognito.API.Models.Requests
{
    public class LoginMfaRequest : MfaCodeConfirmRequest, IUsername, IMfaCode, ISessionID
    {
        public string SessionID { get; set; }
    }
}