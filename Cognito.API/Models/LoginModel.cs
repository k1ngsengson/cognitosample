﻿using Cognito.API.Models.Interfaces;

namespace Cognito.API.Models
{
    public class LoginModel : IUsername
    {
        public string Username { get; set; }
    }
}