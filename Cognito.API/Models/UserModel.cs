﻿using Amazon.CognitoIdentityProvider;
using System;

namespace Cognito.API.Models
{
    public class UserModel
    {
        public string Username { get; set; }
        public DateTime UserCreateDate { get; set; }
        public DateTime UserLastModifiedDate { get; set; }
        public UserStatusType UserStatus { get; set; }
        public bool Enabled { get; set; }
        public string PreferredMfaSetting { get; set; }

        #region Email scope
        public string Email { get; set; }
        public string EmailVerified { get; set; }
        #endregion

        #region Address scope
        public string Address { get; set; }
        #endregion

        #region Phone scope
        public string PhoneNumber { get; set; }
        public string PhoneNumberVerified { get; set; }
        #endregion

        #region Profile scope
        public string BirthDate { get; set; }
        public string LastName { get; set; }
        public string Gender { get; set; }
        public string FirstName { get; set; }
        public string Locale { get; set; }
        public string MiddleName { get; set; }
        public string Name { get; set; }
        public string Nickname { get; set; }
        public string Picture { get; set; }
        public string PreferredUsername { get; set; }
        public string Profile { get; set; }
        public string ZoneInfo { get; set; }
        public string UpdatedAt { get; set; }
        public string Website { get; set; }
        #endregion

        #region Custom attributes
        public string JJUsername { get; set; }
        public string Role { get; set; }
        #endregion
    }

    public class UserLoggedInModel: UserModel
    {
        public string AccessToken { get; set; }
    }
}