﻿namespace Cognito.API.Models.Interfaces
{
    public interface ISessionID
    {
        string SessionID { get; set; }
    }
}