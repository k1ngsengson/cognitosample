﻿namespace Cognito.API.Models.Interfaces
{
    public interface IPassword
    {
        string Password { get; set; }
    }
}