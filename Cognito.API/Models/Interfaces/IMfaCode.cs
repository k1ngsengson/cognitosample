﻿namespace Cognito.API.Models.Interfaces
{
    public interface IMfaCode
    {
        string MfaCode { get; set; }
    }
}