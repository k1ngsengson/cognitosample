﻿namespace Cognito.API.Models.Interfaces
{
    public class UsernameAccessTokenModel : LoginModel, IAccessToken
    {
        public string AccessToken { get; set; }
    }
}