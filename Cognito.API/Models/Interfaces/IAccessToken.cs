﻿namespace Cognito.API.Models.Interfaces
{
    public interface IAccessToken
    {
        string AccessToken { get; set; }
    }
}