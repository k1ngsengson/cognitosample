﻿namespace Cognito.API.Models.Interfaces
{
    public interface IUsername
    {
        string Username { get; set; }
    }
}